CC=g++

SRC_DIR=./src
BIN_DIR=./bin
OBJ_DIR=./obj

SRC=$(wildcard $(SRC_DIR)/*.cpp)
HDR=$(wildcard $(SRC_DIR)/*.hpp) $(wildcard $(SRC_DIR)/*.h)
OBJ=$(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SRC))

DEBUG_EXE=$(BIN_DIR)/debug
RELEASE_EXE=$(BIN_DIR)/cppc

debug: CFLAGS=-g -O0 -Wall
debug: LDFLAGS=

release: CFLAGS=-Wall
release: LDFLAGS=

.PHONY: release debug clean

debug: $(DEBUG_EXE)

release: $(RELEASE_EXE)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp $(HDR)
	$(CC) $(CFLAGS) -c -o $@ $<

$(DEBUG_EXE): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(RELEASE_EXE): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	rm -rf $(OBJ) $(DEBUG_EXE) $(RELEASE_EXE)
