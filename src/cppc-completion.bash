#/usr/bin/bash
_cppc_completions()
{
    if [ "${#COMP_WORDS[@]}" != "2" ]; then
        return
    fi

    COMPREPLY+=($(compgen -W "--directory" "${COMP_WORDS[1]}"))
}

complete -F _cppc_completions cppc
