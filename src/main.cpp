#include <iostream>
#include <fstream>
#include <sstream>
#include <cctype>

void create_files(std::string class_name, std::string write_dir)
{
    // ------------------- HEADER -------------------------------------------
    std::stringstream header_name, full_path;
    header_name << class_name << ".hpp";
    full_path << write_dir << "/" << header_name.str();

    std::ofstream header_file(full_path.str());

    std::string upperName(class_name);

    for (auto & c : upperName) c = std::toupper(c);

    header_file << "#ifndef " << upperName << "_HPP" << std::endl;
    header_file << "#define " << upperName << "_HPP" << std::endl;
    header_file << std::endl;
    header_file << "class " << class_name << std::endl;
    header_file << "{" << std::endl;
    header_file << "    private:" << std::endl;
    header_file << "    protected:" << std::endl;
    header_file << "    public:" << std::endl;
    header_file << "        " << class_name << "(); // Constructor" << std::endl;
    header_file << "        ~" << class_name << "(); // Destructor" << std::endl;
    header_file << "};" << std::endl;
    header_file << std::endl;
    header_file << "#endif";

    header_file.close();
    // ----------------------------------------------------------------------

    // ------------------- SOURCE -------------------------------------------
    std::stringstream source_name;
    source_name << class_name << ".cpp";
    full_path.str("");
    full_path << write_dir << "/" << source_name.str();

    std::ofstream source_file(full_path.str());

    source_file << "#include \"" << header_name.str() << "\"" << std::endl;
    source_file << std::endl;

    source_file << class_name << "::" << class_name << "()" << std::endl; 
    source_file << "{" << std::endl;
    source_file << std::endl;
    source_file << "}" << std::endl;
    source_file << std::endl;

    source_file << class_name << "::~" << class_name << "()" << std::endl; 
    source_file << "{" << std::endl;
    source_file << std::endl;
    source_file << "}" << std::endl;

    source_file.close();
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cout << "Usage: " << argv[0] << " ClassName" << std::endl;
        return 1;
    }

    std::string current;

    bool is_parameter = false;
    std::string flag_name;

    std::string write_dir("./");
    for (int i = 0; i < argc - 1; ++i)
    {
        current = argv[i + 1];
        if (current.size() >= 2 && current[0] == '-' && current[1] == '-')
        {
            flag_name = current.substr(2);
            is_parameter = true;
        }
        else if (is_parameter)
        {
            if (flag_name == "directory")
                write_dir = current;
            is_parameter = false;
        }
        else
        {
            create_files(current, write_dir);
        }
    }

    return 0;
}
