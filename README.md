# NAME
## cppc
cppc stands for cpp class and makes a new class for c++

# SYNOPSIS
cppc --directory [dir\_name] ClassName

# DESCRIPTION
Creates 2 files named ($ClassName).cpp and ($ClassName).hpp.
The hpp (header) file contains the class prototype and constructor/destructor method declarations.
The cpp (source) file includes this header and contains an empty constructor and destructor.

The header file contains #include guards in the form of #ifndef ... #define ... [Class] #endif

# EXAMPLES
cppc --directory src Block

Creates the files Block.cpp and Block.hpp inside the src directory with Block.hpp containing a class prototype for the Block class.

# KNOWN BUGS
bash tab completion is bugged and should not be used :3
